A small repository for sharing the answer validation code I wrote for
the computational science course (TN2513 in Delft).

See the example file for an example of how we use it:

https://gitlab.tudelft.nl/python-for-applied-physics/notebook-answer-validation/blob/master/Example%20Notebook.ipynb